Interroedit lets you simply append "?edit" to a node alias when you wish to be
redirected to the node's edit page.

For example, if node/123 has an alias of my/page, then your browser's address
bar might contain "example.com/my/page". By editing your address bar to
"example.com/my/page?edit" and hitting enter, you will be redirected to
"example.com/node/123/edit".

This could be useful if your template does not render the edit tab.
